package grpc

import (
	"context"
	"log"
	"net"
	"os"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"bitbucket.org/oholovashenko/generator-sv/internal/generator"
	"bitbucket.org/oholovashenko/generator-sv/pkg/protos"
)

type GRPCServer struct {
	s *grpc.Server

	gen *generator.FibonacciGenerator

	log *log.Logger
}

func NewGRPCServer(gen *generator.FibonacciGenerator) *GRPCServer {
	s := &GRPCServer{
		s:   grpc.NewServer(),
		gen: gen,
		log: log.New(os.Stdout, "[grpc] ", 0),
	}

	protos.RegisterGeneratorServiceServer(s.s, s)

	return s
}

func (s *GRPCServer) Serve(lis net.Listener) error {
	return s.s.Serve(lis)
}

func (s *GRPCServer) GetNumber(context.Context, *protos.GetRequest) (*protos.Number, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetNumber not implemented")
}

func (s *GRPCServer) NumbersTopic(req *protos.SubscribeRequest, stream protos.GeneratorService_NumbersTopicServer) error {
	var err error

	_ = req
	s.log.Println("subscribe topic request received")

	emittedNum := 0
	loggerTimer := time.NewTicker(time.Second)
	defer loggerTimer.Stop()

	for {
		g := <-s.gen.C
		err = stream.Send(&protos.Number{Num: g.Number.String(), SequenceNumber: g.SeqNumber})
		if err != nil {
			st, ok := status.FromError(err)
			if !ok {
				break
			}

			if st.Code() == codes.Unavailable {
				s.log.Println("client gone, closing stream")
				break
			}

			s.log.Printf("unhandled error while sending to stream: %s \n", err.Error())
			break
		}

		emittedNum++
		select {
		default:
		case <-loggerTimer.C:
			s.log.Printf("sent %d numbers \n", emittedNum)
			emittedNum = 0
		}
	}

	return nil
}

func (s *GRPCServer) NumbersClientControlledStream(stream protos.GeneratorService_NumbersClientControlledStreamServer) error {
	return status.Errorf(codes.Unimplemented, "NumbersClientControlledStream not implemented")
}

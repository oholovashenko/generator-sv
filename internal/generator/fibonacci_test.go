package generator

import (
	"context"
	"testing"
	"time"

	big "github.com/ncw/gmp"
	"github.com/stretchr/testify/assert"
)

func TestGeneratorBasic(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	g := NewFibonacciGenerator(100)
	g.Run(ctx)

	assert.Equal(t, <-g.C, big.NewInt(int64(0)))
	assert.Equal(t, <-g.C, big.NewInt(int64(1)))

	iterations := 0
	num := new(big.Int)
	var expected102th, _ = new(big.Int).SetString("573147844013817084101", 10)

	brk := time.After(time.Second + 5*time.Millisecond)
LOOP:
	for {
		select {
		case <-brk:
			break LOOP
		case g := <-g.C:
			num = g.Number
			iterations++
		}
	}

	assert.Equal(t, expected102th, num)
	assert.Equal(t, 100, iterations)
}

package generator

import (
	"context"
	"time"

	big "github.com/ncw/gmp"
)

// sure not optimal solution
// production code would recreate/stop/start timer and have less laconic interface
// and other reasons

type FibonacciNumber struct {
	Number    *big.Int
	SeqNumber uint64
}

type FibonacciGenerator struct {
	C      chan FibonacciNumber
	period time.Duration
}

func NewFibonacciGenerator(numps int) *FibonacciGenerator {
	return &FibonacciGenerator{
		C:      make(chan FibonacciNumber, 1),
		period: time.Duration(int(time.Second) / numps),
	}
}

func (g FibonacciGenerator) Run(ctx context.Context) {
	var (
		cn uint64
		a  *big.Int = big.NewInt(int64(0))
		b  *big.Int = big.NewInt(int64(1))

		ticker = time.NewTicker(g.period)
	)

	go func() {
		defer close(g.C)
		defer ticker.Stop()

		for {
			select {
			case <-ticker.C:
				select {
				case g.C <- FibonacciNumber{a, cn}:
					c := new(big.Int)
					c.Add(a, b)
					a = b
					b = c
					cn++
				default:
				}
			case <-ctx.Done():
				return
			}
		}
	}()
}

package cmd

import (
	"github.com/FZambia/viper-lite"
	"github.com/spf13/pflag"
)

func BindFlags() {
	pflag.String("grpc_addr", ":55555", "TCP addr to listen on")
	pflag.Int("generation_speed", 10000, "Number of Fibonacci numbers generated per second")

	pflag.Parse()
	_ = viper.BindPFlags(pflag.CommandLine)
	viper.AutomaticEnv()
}

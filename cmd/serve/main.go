package main

import (
	"context"
	"log"
	"net"
	"os"
	"os/signal"

	"github.com/FZambia/viper-lite"

	"bitbucket.org/oholovashenko/generator-sv/cmd"
	"bitbucket.org/oholovashenko/generator-sv/internal/generator"
	"bitbucket.org/oholovashenko/generator-sv/internal/grpc"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	errC := make(chan error, 1)

	logger := log.New(os.Stdout, "[main] ", 0)

	cmd.BindFlags()

	gen := generator.NewFibonacciGenerator(viper.GetInt("generation_speed"))
	gen.Run(ctx)
	logger.Printf("generator set up and running at %d/s discretion \n", viper.GetInt("generation_speed"))

	addr := viper.GetString("grpc_addr")
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Fatalf("failed to listen port %s", addr)
	}
	defer lis.Close()
	logger.Printf("tcp server listening at %s \n", addr)

	s := grpc.NewGRPCServer(gen)
	go func() { errC <- s.Serve(lis) }()

	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, os.Interrupt)
	select {
	case <-sigC:
		logger.Println("received termination signal")
	case err := <-errC:
		logger.Printf("fatal error occured %s", err.Error())
	}
}

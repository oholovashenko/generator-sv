module bitbucket.org/oholovashenko/generator-sv

go 1.14

require (
	github.com/FZambia/viper-lite v0.0.0-20171108064948-d5a31e6aa18b
	github.com/golang/protobuf v1.4.1
	github.com/mitchellh/mapstructure v1.3.2 // indirect
	github.com/ncw/gmp v1.0.4
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.4.0
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.25.0
)

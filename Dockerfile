FROM tetafro/golang-gcc:latest
RUN apk update && apk add --no-cache git && apk add gmp-dev
WORKDIR /go/src/assignment/gen
COPY . .
RUN go mod download
RUN go mod verify
RUN CGO_ENABLED=1 GO111MODULE=auto GOARCH=amd64 GOOS=linux GOPROXY=direct GOSUMDB=off go build -o /go/bin/generator cmd/serve/main.go
ENTRYPOINT /go/bin/generator